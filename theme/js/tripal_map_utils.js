
////////////////////////////////////////////////////////////
// Utility and helper functions for TripalMap draw routines

var tripalMap = tripalMap || {};

    tripalMap = {

    // Create Event Handlers for mouse
	onSaveToPngMouseOver: function(svg) {

		svg.style("cursor", "pointer");
    	var stpG = svg.append("g")
    		.attr("id", "save_to_png_text");
    	
    	var stp = d3.select("#save_to_png");
    	var stpn = stp.node().getBoundingClientRect();
    	var translateX = stpn.x - svg.node().getBoundingClientRect().x;
    	var translateY = stpn.y - svg.node().getBoundingClientRect().y + stpn.height + 5;

    	stpG.attr("transform", "translate("+ translateX + "," + translateY + ")");
    
    	var stpT = stpG.append("text");
    	stpT.text("Download as a png");

    	var stpTHeight = stpT.node().getBoundingClientRect().height;
    	var stpTWidth = stpT.node().getBoundingClientRect().width;
    	
    	var pad = 8;
    	var stpR = stpG.append("rect");
		stpR.style("fill","#6C7386")
		.attr("width", stpTWidth + pad)
		.attr("height", stpTHeight + pad);

		stpT.remove();
		stpG.append("text")
			.text("Download as a png")
			.style("fill","white")
			.attr("x", (pad/2))
			.attr("y", stpTHeight + (pad/2));

	},
	
	onSaveToPngMouseOut: function(svg) {
		svg.style("cursor", "default");
        
        // Select text by id and then remove
        d3.select("#save_to_png_text").remove(); // Remove text location
	},

	onSaveToPngClick: function(svg, pngFileName) { 
		
		var stp = d3.select("#save_to_png");
		var stpNode = stp.node();
		var stpParent = stp.node().parentNode;
		d3.select("#save_to_png_text").remove();
		stp.remove(); // Remove icon before saving png 
		
		var svgString = tripalMap.getSVGString(svg.node());

		var width = parseInt(svg.style("width"));
		var height = parseInt(svg.style("height"));
		
		tripalMap.svgString2Image( svgString, 2*width, 2*height, 'png', save ); // passes Blob and filesize String to the callback
		function save( dataBlob, filesize ) {
			saveAs( dataBlob, pngFileName ); // FileSaver.js function
		}
		
		stpParent.append(stpNode); // replace icon
	},

    		
	// Below are the functions that handle actual exporting:
	// getSVGString ( svgNode ) and svgString2Image( svgString, width, height, format, callback )
	getSVGString: function(svgNode) {
		svgNode.setAttribute('xlink', 'http://www.w3.org/1999/xlink');
		var cssStyleText = getCSSStyles( svgNode ); // this modifies the font size 
		appendCSS( cssStyleText, svgNode );

		var serializer = new XMLSerializer();
		var svgString = serializer.serializeToString(svgNode);
		// remove the node style attribute
		var style = document.getElementById("tripal_map_style");
		style.parentNode.removeChild(style);

		svgString = svgString.replace(/(\w+)?:?xlink=/g, 'xmlns:xlink='); // Fix root xlink without namespace
		svgString = svgString.replace(/NS\d+:href/g, 'xlink:href'); // Safari NS namespace fix

		return svgString;

		function getCSSStyles( parentElement ) {
			var selectorTextArr = [];

			// Add Parent element Id and Classes to the list
			selectorTextArr.push( '#'+parentElement.id );
			for (var c = 0; c < parentElement.classList.length; c++)
					if ( !contains('.'+parentElement.classList[c], selectorTextArr) )
						selectorTextArr.push( '.'+parentElement.classList[c] );

			// Add Children element Ids and Classes to the list
			var nodes = parentElement.getElementsByTagName("*");
			for (var i = 0; i < nodes.length; i++) {
				var id = nodes[i].id;
				if ( !contains('#'+id, selectorTextArr) )
					selectorTextArr.push( '#'+id );

				var classes = nodes[i].classList;
				for (var c = 0; c < classes.length; c++)
					if ( !contains('.'+classes[c], selectorTextArr) )
						selectorTextArr.push( '.'+classes[c] );
			}

			// Extract CSS Rules
			var extractedCSSText = "";
			for (var i = 0; i < document.styleSheets.length; i++) {
				var s = document.styleSheets[i];
				
				try {
				    if(!s.cssRules) continue;
				} catch( e ) {
			    		if(e.name !== 'SecurityError') throw e; // for Firefox
			    		continue;
			    	}

				var cssText = "";
				var cssRules = s.cssRules;
				for (var r = 0; r < cssRules.length; r++) {
					//if ( contains( cssRules[r].selectorText, selectorTextArr ) )
					if (cssRules[r].selectorText) {
						if (cssRules[r].selectorText == "svg.TripalMap") {
							extractedCSSText += "svg.TripalMap { font: .8em sans-serif; padding-left: 1em;}";
						}                
						else {
							extractedCSSText += cssRules[r].cssText;
						}
					}
				}
			}
			return extractedCSSText;

			function contains(str,arr) {
				return arr.indexOf( str ) === -1 ? false : true;
			}

		}

		function appendCSS( cssText, element ) {
			var styleElement = document.createElement("style");
			styleElement.setAttribute("type","text/css");
			styleElement.setAttribute("id", "tripal_map_style");
			styleElement.innerHTML = cssText;
			var refNode = element.hasChildNodes() ? element.children[0] : null;
			element.insertBefore( styleElement, refNode );
		}
	},


	svgString2Image: function( svgString, width, height, format, callback ) {

		var format = format ? format : 'png';
		var imgsrc = 'data:image/svg+xml;base64,'+ btoa( unescape( encodeURIComponent( svgString ) ) ); // Convert SVG string to data URL

		var canvas = document.createElement("canvas");
		var context = canvas.getContext("2d");

		canvas.width = width;
		canvas.height = height;

		var image = new Image();
		image.onload = function() {
			context.clearRect ( 0, 0, width, height );
			context.drawImage(image, 0, 0, width, height);
			
			  context.save();
			  context.globalCompositeOperation = 'destination-over';
			  context.fillStyle = 'white';
			  context.fillRect(0, 0, canvas.width, canvas.height);
			  // Restore the original context state previously saved
			  context.restore();
			
			canvas.toBlob( function(blob) {
				var filesize = Math.round( blob.length/1024 ) + ' KB';
				if ( callback ) callback( blob, filesize );
			});
		};

		image.src = imgsrc;

	}, 
		
 			
	findCorrespondences: function(data) {
		//output format: {"UBC190":[{"linkageGroup":"LG1", "position":"97.5"},{"linkageGroup":"LG2","position":"85.3"}],
		//                "UBC43":[{"linkageGroup":"LG1","position":"97.5","linkageGroup":"LG2","position":"14"}]}
		
		// hash the markers for the reference chromosome
		var refChrMarkerHash = {};
		data.forEach(function(d) {
			if (!(d.genetic_marker_locus_name in refChrMarkerHash)) {
				refChrMarkerHash[d.genetic_marker_locus_name] = [];
			}
			var markerPos = "";
			if (d.genetic_marker_type == "QTL") {
				if (d.hasOwnProperty("marker_qtl_peak")) {
					markerPos = d.marker_qtl_peak;
				}
				if (d.hasOwnProperty("marker_start_pos")) {
					markerPos = d.marker_start_pos;
				}
			}
			else {
				markerPos = d.marker_start_pos; 
			}
			refChrMarkerHash[d.genetic_marker_locus_name].push({"linkageGroup":d.linkage_group,"position":markerPos, "feature_id": d.feature_id, "feature_url": d.feature_url});
		});  
		
		// keep only the markers that appear in more than one linkage group.
		var markerCorrespondences = {};
		for (var k in refChrMarkerHash) {
			if (refChrMarkerHash.hasOwnProperty(k)) {
				if (refChrMarkerHash[k].length > 1) {
					markerCorrespondences[k] = refChrMarkerHash[k];
				}
			}
		}

		return markerCorrespondences;
	},

		
	orderFeaturemapToc: function( featuremapId) {
		var sidebar = document.getElementById('chado_featuremap-tripal-toc-pane');
		if (sidebar) {
			
			// Order the TOC items
			var div = sidebar.getElementsByTagName('div');
			var ordered_items = [];
			var index = 2;

			for (var i = 0; i < div.length; i ++) {

				var as = div[i].getElementsByTagName('a');
				if (as.length === 0) {
					continue;
				}
				var title = as[0].innerHTML.trim();
				if (title.toLowerCase() === 'view map') {
					// set the map viewer pane tab to link to a clean mapviewer window
					var mapviewerHref = Drupal.settings.baseUrl+"/mapviewer/"+featuremapId;
					as[0].setAttribute('href', mapviewerHref);
					as[0].setAttribute('target', '_blank');
					if(div[i].hasAttribute('class')) {
						// override class="tripal_toc_list_item", to avoid interference from tripal js 
						div[i].setAttribute('class', 'map-viewer-item');
					}
					if(as[0].hasAttribute('class')) {
						// override class="tripal_toc_list_item_link", to avoid interference from tripal js 
						as[0].setAttribute('class', 'map-viewer-link');
					}
				}
				
				
				// Data
				if (title.toLowerCase() === 'map overview' ) {
					ordered_items [0] = jQuery(div[i]).clone();
				} else if (title.toLowerCase() === 'view map') {
					ordered_items [1] = jQuery(div[i]).clone();
				}		
				else{
					ordered_items [index] = jQuery(div[i]).clone();
					index = index + 1;
				}				
			}
			
			// Output
			sidebar.innerHTML = '';			
			for (var i = 0; i < ordered_items.length; i ++) {
				if (typeof(ordered_items[i]) !== 'undefined') {
					jQuery(sidebar).append(ordered_items[i]);
				}
			}
		}
	},
    		
 	d3VersionFour: function() {
		return false;
	},
	
	convertTextColor: function(color) {
		if (color === "#FFFF67") {
			// change yellow to a darker, more visible shade for text
			return "#8C96A8";
		} 
		else {
			return color; 
		}
	},	
	

	sanitizeChrUrlId: function(linkageGroup) {
		var str = linkageGroup;
		var res = str.replace(/\+/g, "%2B");
		return res;
	},


	wrap: function(text, width) {
		text.each(function() {
			var textObj = d3.select(this);
			var textContent = textObj.text();
			var lineHeight = textObj.style('line-height') || 0;
			var rectX = textObj.attr('x') || 0;
			var rectY = textObj.attr('y') || 0;
			var dy = parseFloat(lineHeight) || 0;
			
			var lineObj = textObj.text(null);
			let lines = [];
			var line = '';
			textContent.split('').forEach(character => {
				line += character;
				lineObj = lineObj.text(line);
				if (lineObj.node().getComputedTextLength() >= (width - 10)) {
					// write the line, purge the line buffer
					lines.push(line);
					line = '';
					lineObj = textObj.text(null);
				}
			});
			lines.push(line);
			
			var numLines = lines.length;
			textObj = textObj.text(null); // clear the text in the main text container
			textObj.append('tspan').text(lines[0]); // the first tspan takes the parent container's relational position
			for (var cur = 1; cur < numLines; cur++) {
				line = lines[cur];
				textObj.append('tspan')
					.attr('x', rectX)
					.attr( 'y', parseFloat(rectY) + dy*(cur))
					.text(line);
			}
		});
	},
	
	lGNameReduction: function(linkageGroup, mapNameIn) {

		if ((typeof linkageGroup != 'string') || (typeof mapNameIn != 'string')) {
			return linkageGroup;
		}
		
		var linkageGroupName = linkageGroup.replace(/['"]+/g, '');
		
		// test the linkageGroup name for map name duplicate inclusion
		var mapName = mapNameIn.replace(/['"]+/g, '');
	
		var mapNameIndex = linkageGroupName.indexOf(mapName); // -1 if never occurs
		if ((mapNameIndex > -1) && ((mapNameIndex + mapName.length) < linkageGroupName.length)) {
			// If the exact mapName string occurs in the linkage group name, remove it
			linkageGroupName = linkageGroupName.substr( mapNameIndex + mapName.length+1, linkageGroupName.length - 1);
		}
		return linkageGroupName;
	},

	// Sorts the linkage group names according to letter, symbol, base-ten integer, and roman numeral
	// linkageGroupNames: array of all linkage groups names in a map
	customLgSort: function(linkageGroupNames) {
		
		// determine if roman numerals exist
		var romanNum = tripalMap.containsRomanNumeral(linkageGroupNames); // create objects of original lg name and tokenized lg name. 
		
		var lgAr = [];
		linkageGroupNames.forEach(function(lgName) {
			var token = {"name": lgName, "tokenArray": tripalMap.tokenizeName(lgName, romanNum)};
			lgAr.push(token);
		});
		
		// sort names array by the token array values
		lgAr.sort(tripalMap.compareTokenizedNames);
		var lgNamesAr = lgAr.map(x => x.name);
		return lgNamesAr;
		
	},
	
	containsRomanNumeral: function(linkageGroupNames) {
		
		let lgNamesAr = linkageGroupNames;
		
		// determine if roman numerals exist based on string patterns containing 2 or more sample multi-char substrings
		var ret = false;
		var occurrences = 0;
		let romanNums = ['II', 'III', 'IV', 'VI', 'VII'];
		for (let lgName of lgNamesAr) {
			for (let rNum of romanNums) {
				if (lgName.includes(rNum)) {
					// at least one from the roman numeral test set occurs in the linkage group name
					occurrences += 1;
					break;
				}
			}
			if (occurrences > 2) {
				ret = true;
				break;
			}
		}
		return ret;
		
	},
	
	compareTokenizedNames: function(a, b) {
		// returns 1 if a is in ascending order before b, in value of char, integer and roman numeric precedence. else returns 0.
		ret = 0; // a == b, return 0
		var count = 0;
		var aLen = a.tokenArray.length;
		var bLen = b.tokenArray.length;
		var minLen = aLen < bLen ? aLen : bLen;
		
		while (count < minLen) {
			var cha = a.tokenArray[count];
			var chb = b.tokenArray[count];
			// compare token at each index count until find a pair not of equal value
			if (cha > chb) {
				ret = 1; // b < a, return 1
				break;
			}
			else if (cha < chb) {
				ret = -1; // a < b, return -1
				break;
			}
			count += 1;
			
		}
		
		if ((ret == 0) && (aLen != bLen)) {
			// the strings are equal in value but lengths are different. Assign the shorter string a lesser value
			ret = aLen < bLen ? -1 : bLen < aLen ? 1 : 0;
		}
		
		return ret;
		
	},
	
	tokenizeName: function(name, containsRomanNum) {
		
		var nameTokenAr = [];
		
		// If roman numerals exist, remove them, convert to integer strings and re-insert in originial position
		// in the next step convert to integer and apply ascii mapping
		if (containsRomanNum) {
			var romanBuf = "";
			var startPos = 0;
			var curIndex = 0;
			var romanChars = ['I', 'V', 'X'];
			name.split('').forEach(character => {
				if (romanChars.indexOf(character) >= 0) {
					// character is part of roman character set
					if (romanBuf.length == 0) {
						startPos = curIndex; // this is the first character in the roman number
					}
					romanBuf += character;
					var nextCharIsRoman = false;
					if ((curIndex+1) < name.length) {
						if (romanChars.indexOf(name.charAt(curIndex+1)) >= 0) {
							// the character was found in the roman characters lookup table
							nextCharIsRoman = true;
						}
					}
					if ((!nextCharIsRoman) || ((curIndex + 1) == name.length)) {
						// the character after is not part of the roman numeral character set, or this is the end of the string,
						// so the end of the roman numeral is reached. convert the roman numeral to an integer
						var asciiPlusIntBase = 10000;
						var romanIntVal = tripalMap.fromRomanToInt(romanBuf) + asciiPlusIntBase;
						// delete the roman characters from the name and insert the integer characters to replace them in the same position
						name = name.substr(0, startPos) + romanIntVal.toString() + name.substr(romanBuf.length);
						romanBuf = ""; //clear the buffer
					}
				}
				curIndex += 1; // increment the index into the string array
			});
		}
		
		var nameL = name.toLowerCase(); // set all chars to lower case, as sort order is case independent.
		var numBuf = "";
		var index = 0;
		nameL.split('').forEach(character => {
			if (!isNaN(character)) {
				// character is a number. Add the number character to the number buffer
				numBuf += character;
				var nextCharIsNum = false;
				if ((index+1) < name.length) {
					// test if the char after is a number, checking we have not exceeded the length of the string
					nextCharIsNum = !isNaN(nameL.charAt(index + 1));
				}
				if ((!nextCharIsNum) || ((index + 1) == nameL.length)) {
					// the char after is not a number, or this is the end of the string,
					// so the end of the number cluster is reached. convert to integer, apply the ascii Base and add as token to the token array
					var asciiBase = 127;
					var intCode = parseInt(numBuf) + asciiBase;
					nameTokenAr.push(intCode);
					numBuf = ""; //clear the buffer
				}
			}
			else {
				// character is a letter or symbol. get the ascii code and add as token
				var charCode = character.charCodeAt();
				nameTokenAr.push(charCode);
			}
			index += 1; // increment the index into the string array
		});
		
		return nameTokenAr;
		
	},
	
	// Original Code from https://www.selftaughtjs.com/algorithm-sundays-converting-roman-numerals/
	fromRomanToInt:	function(str) {
		
		var intVal = 0;
		
		// the result is now a number, not a string
		var decimal = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
		var roman = ["M", "CM","D","CD","C", "XC", "L", "XL", "X","IX","V","IV","I"];
		for (var i = 0; i <= decimal.length; i++) {
			while (str.indexOf(roman[i]) === 0)	{
				intVal += decimal[i];
				str = str.replace(roman[i],'');
			}
		}
		return intVal;
		
	},
	
	encodeHtmlIdAttrib: function(unsafe) {
		// remove all chars from the linkage group name that are not accepted in an Html id attribute
	    return unsafe.replace(/[^a-zA-Z0-9-_]/g, "_");
	 },

};
