
/* 
 * File: tripal_dotplot.js
 * Obtain settings from MapViewer PHP form, call Dot Plot draw to display selected chromosomes.
 */

(function($) {
  Drupal.behaviors.tripal_map_dotplot_Behavior = {
    attach: function (context, settings) {
    	
    	$('#select_fieldset_dotplot').once('select_fieldset_dotplot', function() {

    	var primMapId = 			Drupal.settings.mapDotPlotJS.primary_map_id;	
    	var primMap =     			Drupal.settings.mapDotPlotJS.primary_map;
    	var primLinkageGroup =   	Drupal.settings.mapDotPlotJS.prim_linkage_group; 
    	var primGeneticFeatures = 	Drupal.settings.mapDotPlotJS.dotplot_prim_genetic_features;

    	var secMapId =       		Drupal.settings.mapDotPlotSecJS.secondary_map_id;                   	
    	var secMap =       			Drupal.settings.mapDotPlotSecJS.secondary_map;                   	
       	var secLinkageGroup =  		Drupal.settings.mapDotPlotSecJS.secondary_linkage_group;
    	var secGeneticFeatures =  	Drupal.settings.mapDotPlotSecJS.dotplot_sec_genetic_features;

    	var filteredPrimGeneticFeatures = [];
    	primGeneticFeatures.forEach(function(d) {
    		if (d.linkage_group == primLinkageGroup) {
    			filteredPrimGeneticFeatures.push(d);
    		}
    	});
    	
    	var filteredSecGeneticFeatures = [];
    	secGeneticFeatures.forEach(function(d) {
    		if (d.linkage_group == secLinkageGroup) {
    			filteredSecGeneticFeatures.push(d);
    	    }
    	});

    	
    	var featuresCombined = filteredPrimGeneticFeatures.concat(filteredSecGeneticFeatures);
    	
    	var container =  "#select_fieldset_dotplot";
    	$(container).before('<div></div><div id ="select_fieldset_dotplot_svg" width="100%"></div>');
    	$(container).remove(); // positional indicator container no longer needed
    	
    	var corres = tripalMap.findCorrespondences(featuresCombined);
    	dotplot.drawDotplotPlotly(primMapId, primMap, primLinkageGroup, secMapId, secMap, secLinkageGroup, corres);

    });
    }
  };
})(jQuery);

